/*
 * Ticket API of Go [Distributor]
 * Ticket API of Go [Distributor]
 *
 * OpenAPI spec version: 1.0.0
 * Contact: go.help@derbysoft.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.derbysoft.go.ticket.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * AttractionDetail
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-13T07:07:20.778Z[GMT]")
public class AttractionDetail extends AttractionSummary {
    @SerializedName("ticketProducts")
    private List<TicketProductSummary> ticketProducts = null;

    public AttractionDetail ticketProducts(List<TicketProductSummary> ticketProducts) {
        this.ticketProducts = ticketProducts;
        return this;
    }

    public AttractionDetail addTicketProductsItem(TicketProductSummary ticketProductsItem) {
        if (this.ticketProducts == null) {
            this.ticketProducts = new ArrayList<TicketProductSummary>();
        }
        this.ticketProducts.add(ticketProductsItem);
        return this;
    }

    /**
     * Get ticketProducts
     *
     * @return ticketProducts
     **/
    @Schema(description = "")
    public List<TicketProductSummary> getTicketProducts() {
        return ticketProducts;
    }

    public void setTicketProducts(List<TicketProductSummary> ticketProducts) {
        this.ticketProducts = ticketProducts;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AttractionDetail attractionDetail = (AttractionDetail) o;
        return Objects.equals(this.ticketProducts, attractionDetail.ticketProducts) &&
                super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketProducts, super.hashCode());
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AttractionDetail {\n");
        sb.append("    ").append(toIndentedString(super.toString())).append("\n");
        sb.append("    ticketProducts: ").append(toIndentedString(ticketProducts)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
