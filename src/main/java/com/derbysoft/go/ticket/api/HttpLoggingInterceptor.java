package com.derbysoft.go.ticket.api;

import com.squareup.okhttp.*;
import okio.Buffer;
import okio.BufferedSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


public class HttpLoggingInterceptor implements Interceptor {

    private static final Charset UTF8 = StandardCharsets.UTF_8;

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpLoggingInterceptor.class);
    private static final Logger ACCESS_LOGGER = LoggerFactory.getLogger("http.AccessLog");
    private static final Logger STREAM_LOGGER = LoggerFactory.getLogger("http.StreamLog");

    private boolean logHeaders = false;

    public HttpLoggingInterceptor(boolean logHeaders) {
        this.logHeaders = logHeaders;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        traceRequestHeaders(request);

        String requestId = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        traceRequestStreamLog(requestId, request);

        long startNs = System.nanoTime();
        Response response = chain.proceed(request);
        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);

        traceAccessLog(chain, request, response, tookMs);
        traceResponseStreamLog(requestId, response);
        traceResponseHeaders(response);

        return response;
    }

    protected void traceRequestHeaders(Request request) {
        if (!logHeaders) {
            return;
        }

        LOGGER.debug("HTTP Request Data");
        Headers requestHeaders = request.headers();
        for (int i = 0, count = requestHeaders.size(); i < count; i++) {
            String name = requestHeaders.name(i);
            LOGGER.debug(" >> " + name + ": " + requestHeaders.value(i));
        }
    }

    protected void traceResponseHeaders(Response response) {
        if (!logHeaders) {
            return;
        }

        LOGGER.debug("HTTP Response Data");
        Headers responseHeaders = response.headers();
        for (int i = 0, count = responseHeaders.size(); i < count; i++) {
            LOGGER.debug(" << " + responseHeaders.name(i) + ": " + responseHeaders.value(i));
        }
    }

    protected void traceAccessLog(Chain chain, Request request, Response response, long tookMs) throws IOException {
        Connection connection = chain.connection();
        Protocol protocol = connection != null ? connection.getProtocol() : Protocol.HTTP_1_1;

        URL url = request.url();
        String requestLine = url.getPath();
        if (url.getQuery() != null && url.getQuery().length() > 0) {
            requestLine = requestLine + "?" + url.getQuery();
        }
        ACCESS_LOGGER.debug("{}|{}:{} \"{} {} {}\" {} {} rt={} {}",
                connection == null ? url.getHost() : connection.getSocket().getInetAddress().getHostAddress(),
                connection == null ? url.getHost() : connection.getSocket().getInetAddress().getHostAddress(),
                connection == null ? (url.getPort() == -1 ? (request.isHttps() ? 443 : 80) : url.getPort()) : connection.getSocket().getPort(),
                request.method(),
                requestLine,
                protocol.toString(),
                response.code(),
                request.body() == null ? 0 : request.body().contentLength(),
                tookMs,
                response.body().contentLength()
        );
    }

    protected void traceRequestStreamLog(String requestId, Request request) throws IOException {
        String requestStream = toString(request.body());
        if (requestStream != null) {
            STREAM_LOGGER.info(requestId + " request >| " + requestStream);
        }
    }

    protected void traceResponseStreamLog(String requestId, Response response) throws IOException {
        String responseStream = toString(response.body());
        STREAM_LOGGER.info(requestId + " response <| " + responseStream);
    }

    protected String toString(RequestBody body) throws IOException {
        if (body == null) {
            return null;
        }

        Charset charset = UTF8;
        MediaType contentType = body.contentType();
        if (contentType != null) {
            contentType.charset(charset);
        }

        Buffer buffer = new Buffer();
        body.writeTo(buffer);
        return buffer.readString(charset);
    }

    protected String toString(ResponseBody body) throws IOException {
        Charset charset = UTF8;
        MediaType contentType = body.contentType();
        if (contentType != null) {
            charset = contentType.charset(UTF8);
        }

        BufferedSource source = body.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();
        return buffer.clone().readString(charset);
    }

}
