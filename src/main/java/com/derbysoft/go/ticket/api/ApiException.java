package com.derbysoft.go.ticket.api;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-01-15T03:32:33.414Z")
public class ApiException extends Exception {

    public static final String ERROR_CODE_UNKNOWN = "Unknown";
    public static final String ERROR_CODE_NETERROR = "NetError";
    public static final String ERROR_CODE_INVALIDFIELD = "InvalidField";

    private String code = ERROR_CODE_UNKNOWN;

    public ApiException(String code, String message) {
        super(message);
        this.code = code;
    }

    public ApiException(String code, String message, Throwable throwable) {
        super(message, throwable);
        this.code = code;
    }

    public ApiException(String message) {
        this(ERROR_CODE_UNKNOWN, message);
    }

    public String getCode() {
        return code;
    }
}
