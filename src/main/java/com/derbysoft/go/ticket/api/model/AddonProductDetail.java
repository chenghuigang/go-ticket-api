/*
 * Ticket API of Go [Distributor]
 * Ticket API of Go [Distributor]
 *
 * OpenAPI spec version: 1.0.0
 * Contact: go.help@derbysoft.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.derbysoft.go.ticket.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * AddonProductDetail
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-13T07:07:20.778Z[GMT]")
public class AddonProductDetail {
    @SerializedName("addonProductId")
    private String addonProductId = null;

    @SerializedName("addonProductName")
    private String addonProductName = null;

    @SerializedName("description")
    private String description = null;

    @SerializedName("rates")
    private List<TicketProductDetailRates> rates = new ArrayList<TicketProductDetailRates>();

    public AddonProductDetail addonProductId(String addonProductId) {
        this.addonProductId = addonProductId;
        return this;
    }

    /**
     * addon product id
     *
     * @return addonProductId
     **/
    @Schema(example = "MemoryMaker", required = true, description = "addon product id")
    public String getAddonProductId() {
        return addonProductId;
    }

    public void setAddonProductId(String addonProductId) {
        this.addonProductId = addonProductId;
    }

    public AddonProductDetail addonProductName(String addonProductName) {
        this.addonProductName = addonProductName;
        return this;
    }

    /**
     * addon product name
     *
     * @return addonProductName
     **/
    @Schema(example = "Memory Maker", description = "addon product name")
    public String getAddonProductName() {
        return addonProductName;
    }

    public void setAddonProductName(String addonProductName) {
        this.addonProductName = addonProductName;
    }

    public AddonProductDetail description(String description) {
        this.description = description;
        return this;
    }

    /**
     * addon product description
     *
     * @return description
     **/
    @Schema(example = "Memory Maker", description = "addon product description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AddonProductDetail rates(List<TicketProductDetailRates> rates) {
        this.rates = rates;
        return this;
    }

    public AddonProductDetail addRatesItem(TicketProductDetailRates ratesItem) {
        this.rates.add(ratesItem);
        return this;
    }

    /**
     * Get rates
     *
     * @return rates
     **/
    @Schema(required = true, description = "")
    public List<TicketProductDetailRates> getRates() {
        return rates;
    }

    public void setRates(List<TicketProductDetailRates> rates) {
        this.rates = rates;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddonProductDetail addonProductDetail = (AddonProductDetail) o;
        return Objects.equals(this.addonProductId, addonProductDetail.addonProductId) &&
                Objects.equals(this.addonProductName, addonProductDetail.addonProductName) &&
                Objects.equals(this.description, addonProductDetail.description) &&
                Objects.equals(this.rates, addonProductDetail.rates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addonProductId, addonProductName, description, rates);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AddonProductDetail {\n");

        sb.append("    addonProductId: ").append(toIndentedString(addonProductId)).append("\n");
        sb.append("    addonProductName: ").append(toIndentedString(addonProductName)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    rates: ").append(toIndentedString(rates)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
