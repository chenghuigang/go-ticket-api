/*
 * Ticket API of Go [Distributor]
 * Ticket API of Go [Distributor]
 *
 * OpenAPI spec version: 1.0.0
 * Contact: go.help@derbysoft.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.derbysoft.go.ticket.api.api;


import com.derbysoft.go.ticket.api.*;
import com.derbysoft.go.ticket.api.model.ShoppingAttractionRequest;
import com.derbysoft.go.ticket.api.model.ShoppingAttractionResponse;
import com.derbysoft.go.ticket.api.model.ShoppingProductRequest;
import com.derbysoft.go.ticket.api.model.ShoppingProductResponse;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingApi {
    private ApiClient apiClient;

    public ShoppingApi() {
        this(Configuration.getDefaultApiClient());
    }

    public ShoppingApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for shoppingAttractionPost
     *
     * @param body                    shoping attraction request (optional)
     * @param progressListener        Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call shoppingAttractionPostCall(ShoppingAttractionRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;

        // create path and map variables
        String localVarPath = "/shopping/attraction";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if (progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[]{"accessToken"};
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call shoppingAttractionPostValidateBeforeCall(ShoppingAttractionRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        com.squareup.okhttp.Call call = shoppingAttractionPostCall(body, progressListener, progressRequestListener);
        return call;


    }

    /**
     * Query rate summary (lowest and hightest rates) of ticket products of attraction
     * Distributor could shop DerbySoft ARI cache to query available rates of ticket products of attraction
     *
     * @param body shoping attraction request (optional)
     * @return ShoppingAttractionResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ShoppingAttractionResponse shoppingAttractionPost(ShoppingAttractionRequest body) throws ApiException {
        ApiResponse<ShoppingAttractionResponse> resp = shoppingAttractionPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     * Query rate summary (lowest and hightest rates) of ticket products of attraction
     * Distributor could shop DerbySoft ARI cache to query available rates of ticket products of attraction
     *
     * @param body shoping attraction request (optional)
     * @return ApiResponse&lt;ShoppingAttractionResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ShoppingAttractionResponse> shoppingAttractionPostWithHttpInfo(ShoppingAttractionRequest body) throws ApiException {
        com.squareup.okhttp.Call call = shoppingAttractionPostValidateBeforeCall(body, null, null);
        Type localVarReturnType = new TypeToken<ShoppingAttractionResponse>() {
        }.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Query rate summary (lowest and hightest rates) of ticket products of attraction (asynchronously)
     * Distributor could shop DerbySoft ARI cache to query available rates of ticket products of attraction
     *
     * @param body     shoping attraction request (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call shoppingAttractionPostAsync(ShoppingAttractionRequest body, final ApiCallback<ShoppingAttractionResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = shoppingAttractionPostValidateBeforeCall(body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ShoppingAttractionResponse>() {
        }.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for shoppingProductPost
     *
     * @param body                    shopping product request (optional)
     * @param progressListener        Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call shoppingProductPostCall(ShoppingProductRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;

        // create path and map variables
        String localVarPath = "/shopping/product";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if (progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[]{"accessToken"};
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call shoppingProductPostValidateBeforeCall(ShoppingProductRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        com.squareup.okhttp.Call call = shoppingProductPostCall(body, progressListener, progressRequestListener);
        return call;


    }

    /**
     * Query rates of ticket product
     * Distributor could shop DerbySoft ARI cache to query available rates of ticket product
     *
     * @param body shopping product request (optional)
     * @return ShoppingProductResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ShoppingProductResponse shoppingProductPost(ShoppingProductRequest body) throws ApiException {
        ApiResponse<ShoppingProductResponse> resp = shoppingProductPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     * Query rates of ticket product
     * Distributor could shop DerbySoft ARI cache to query available rates of ticket product
     *
     * @param body shopping product request (optional)
     * @return ApiResponse&lt;ShoppingProductResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ShoppingProductResponse> shoppingProductPostWithHttpInfo(ShoppingProductRequest body) throws ApiException {
        com.squareup.okhttp.Call call = shoppingProductPostValidateBeforeCall(body, null, null);
        Type localVarReturnType = new TypeToken<ShoppingProductResponse>() {
        }.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Query rates of ticket product (asynchronously)
     * Distributor could shop DerbySoft ARI cache to query available rates of ticket product
     *
     * @param body     shopping product request (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call shoppingProductPostAsync(ShoppingProductRequest body, final ApiCallback<ShoppingProductResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = shoppingProductPostValidateBeforeCall(body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ShoppingProductResponse>() {
        }.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
