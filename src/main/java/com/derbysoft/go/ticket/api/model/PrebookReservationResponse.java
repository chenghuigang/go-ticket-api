/*
 * Ticket API of Go [Distributor]
 * Ticket API of Go [Distributor]
 *
 * OpenAPI spec version: 1.0.0
 * Contact: go.help@derbysoft.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.derbysoft.go.ticket.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

/**
 * PrebookReservationResponse
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-13T07:07:20.778Z[GMT]")
public class PrebookReservationResponse extends Response {
    @SerializedName("bookingToken")
    private String bookingToken = null;

    public PrebookReservationResponse bookingToken(String bookingToken) {
        this.bookingToken = bookingToken;
        return this;
    }

    /**
     * booking token used for book
     *
     * @return bookingToken
     **/
    @Schema(example = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c", required = true, description = "booking token used for book")
    public String getBookingToken() {
        return bookingToken;
    }

    public void setBookingToken(String bookingToken) {
        this.bookingToken = bookingToken;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PrebookReservationResponse prebookReservationResponse = (PrebookReservationResponse) o;
        return Objects.equals(this.bookingToken, prebookReservationResponse.bookingToken) &&
                super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingToken, super.hashCode());
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class PrebookReservationResponse {\n");
        sb.append("    ").append(toIndentedString(super.toString())).append("\n");
        sb.append("    bookingToken: ").append(toIndentedString(bookingToken)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
