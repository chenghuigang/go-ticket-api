/*
 * Ticket API of Go [Distributor]
 * Ticket API of Go [Distributor]
 *
 * OpenAPI spec version: 1.0.0
 * Contact: go.help@derbysoft.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.derbysoft.go.ticket.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ReadReservationResponse
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-11-13T07:07:20.778Z[GMT]")
public class ReadReservationResponse extends Response {
    @SerializedName("reservations")
    private List<Object> reservations = new ArrayList<Object>();

    public ReadReservationResponse reservations(List<Object> reservations) {
        this.reservations = reservations;
        return this;
    }

    public ReadReservationResponse addReservationsItem(Object reservationsItem) {
        this.reservations.add(reservationsItem);
        return this;
    }

    /**
     * Get reservations
     *
     * @return reservations
     **/
    @Schema(required = true, description = "")
    public List<Object> getReservations() {
        return reservations;
    }

    public void setReservations(List<Object> reservations) {
        this.reservations = reservations;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReadReservationResponse readReservationResponse = (ReadReservationResponse) o;
        return Objects.equals(this.reservations, readReservationResponse.reservations) &&
                super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservations, super.hashCode());
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ReadReservationResponse {\n");
        sb.append("    ").append(toIndentedString(super.toString())).append("\n");
        sb.append("    reservations: ").append(toIndentedString(reservations)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
